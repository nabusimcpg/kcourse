//var greeting: String? = null

// fun getGreeting() = "Hello Kotlin"  // Ok too


fun getGreeting(): String {
    return "Hello Kotlin"
}

fun sayHello(): Unit {
    println(getGreeting())
}

/*fun sayHello2(itemToGreet: String) {
    val msg = "Hello $itemToGreet"
    println(msg)
}*/

fun sayHello2(itemToGreet: String) = println("Hello $itemToGreet")

fun sayHello3(greeting: String, itemsToGreet: List<String>) {
    itemsToGreet.forEach { itemToGreet ->
        println("$greeting $itemToGreet")
    }
}

fun sayHello4(greeting: String, vararg itemsToGreet: String) {
    itemsToGreet.forEach { itemToGreet ->
        println("$greeting $itemToGreet")
    }
}

fun greetPerson(greeting: String = "Hello", name: String = "Kotlin") = println("$greeting $name")

fun main() {
    println("Hello World")
    var name: String = "Cristian"

    /*   if (greeting != null) {
           println(greeting)
       } else {
           println("Hi")
       }

       // Select case
       when (greeting) {
           null -> println("Hi")
           else -> println(greeting)
       }

       val greetingToPrint = when (greeting) {
           null -> "Hi"
           else -> greeting
       }

       println(greetingToPrint)*/
    println(name)
//    println(getGreeting())
    sayHello()
    sayHello2("CPG")

    //

    val interestingThings = arrayOf("Kotlin", "Programming", "Comic Books")
    println(interestingThings.size)
    println(interestingThings[0])
    println(interestingThings.get(0))

    for (iT in interestingThings) {
        println(iT)
    }

    interestingThings.forEach {
        println(it)
    }

    interestingThings.forEach { interestingThin ->
        println(interestingThin)
    }

    interestingThings.forEach(::println)

    interestingThings.forEachIndexed { index, interestingThing ->
        println("$interestingThing is at index $index")
    }

    // val interestingThings = listOf("Kotlin", "Programming", "Comic Books")
    val interestingThings2 = mutableListOf("Kotlin", "Programming", "Comic Books")
    interestingThings2.add("Dogs")

    // val map = mapOf(1 to "a", 2 to "b", 3 to "c")
    val map = mutableMapOf(1 to "a", 2 to "b", 3 to "c")
    map.forEach { key, value -> println("$key -> $value") }
    map.put(4, "d")

    sayHello3("HI", interestingThings2)
    sayHello4("Hi", "Kotlin", "Programming", "Comic Books")
    sayHello4("Hi2", *interestingThings)
    greetPerson()

    // val person = Person("Cristian", "Perez")
    val person = Person()
//    person.nickName = "nabusim"
//    println(person.nickName)
    person.printInfo()


}