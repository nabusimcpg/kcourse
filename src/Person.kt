//class Person(_firstName: String, _lastName: String) {
//    val firstName: String = _firstName
//    val lastName: String = _lastName
///*    val firstName: String
//    val lastName: String
//
//    init {
//        firstName = _firstName
//        lastName = _lastName
//    }*/
//}

class Person(val firstName: String, val lastName: String) {
    init {
        println("init 1")
    }

    constructor() : this("Peter", "Parker") {
        println("Secundary constructor")
    }

    init {
        println("init 2")
    }

    var nickName: String? = null
        set(value) {
            field = value
            println("the nickNamne is $value")
        }
        get() {
            println("the returned value is $field")
            return field
        }

    fun printInfo() {
        val nickNameToPrint = nickName ?: "no nickname"
        println("$firstName ($nickNameToPrint) $lastName")
    }
}